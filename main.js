'use strict';

const {
    app,
    Tray,
    Menu,
    BrowserWindow
} = require("electron");
const Reminder = require("./app/Reminder");
const configuration = require("./app/configuration");
const ipc = require("electron").ipcMain;

let reminder = null;
let settingsWindow = null;
app.isQuitting = false;
let trayMenu = null;
let appIcon = null;
let iconPath = null;
let isEnabled = configuration.readSettings("enableReminder", true);

function init() {
    reminder = new Reminder();
    if (isEnabled) {
        reminder.startReminder();
    }
    iconPath = __dirname + "/app/assets/icons/refresh_icon.png"
    if (process.platform === "darwin") {
        iconPath = __dirname + "/app/assets/icons/refresh_icon24Template.png"
    }
    appIcon = new Tray(iconPath);
    appIcon.setHighlightMode("always");
    var trayMenuTemplate = [{
        label: "Refresh",
        click: function () {
            reminder.showReminder()
        }
    }, {
        label: "Settings",
        click: function () {
            console.log("Settings");
            settingsWindow.show();
        }
    }, {
        label: "Quit",
        click: function () {
            console.log("close");
            if (isEnabled) {
                reminder.stopReminder();
            }
            app.isQuitting = true;
            app.quit();
        }
    }];
    trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
    appIcon.setToolTip("I'mma help you live longer ;p");
    appIcon.setContextMenu(trayMenu);
    appIcon.on("double-click", () => {
        // console.log("Settings on double click");
        settingsWindow.show();
    });
    createSettingsWindow();
}

ipc.on("config-change", () => {
    console.log("Setting change detected");
    if (isEnabled) {
        reminder.stopReminder();
    }
    isEnabled = configuration.readSettings("enableReminder", true);
    if (isEnabled) {
        reminder.startReminder();
    }
});

function createSettingsWindow() {
    if (settingsWindow != null) {
        return; // Open only one instance of settings
    }
    settingsWindow = new BrowserWindow({
        show: false,
        icon: __dirname + "/app/assets/icons/refresh_icon.ico"
    });
    settingsWindow.setMenu(null);
    settingsWindow.loadFile("control-panel.html");
    settingsWindow.on("ready-to-show", function () {
        settingsWindow.show();
        console.log("Args: " + process.argv);
        if (process.argv.indexOf("--debug") > -1) {
            settingsWindow.openDevTools();
        }
    });
    settingsWindow.on("minimize", function (event) {
        // settingsWindow = null;
        event.preventDefault();
        settingsWindow.hide();
        if (process.platform === "darwin") {
            console.log("Hiding the dock icon");
            if (app.dock.isVisible()) {
                app.dock.hide();
            }
        }
    });
    settingsWindow.on("close", function (event) {
        // settingsWindow = null;
        if (!app.isQuitting && isEnabled) {
            event.preventDefault();
            settingsWindow.minimize();
            return false;
        } else {
            settingsWindow = null;
        }
    });
}

// app.on("ready", createWindow);
app.on("ready", init);