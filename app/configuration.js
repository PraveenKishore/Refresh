'use strict';

var nconf = require("nconf").file({file: getUserHome() + "/reminder-config.json"});
var ipc = require("electron").ipcRenderer;

const DEFAULT_REMINDER_TIME = 20;

function saveSettings(settingKey, settingValue) {
    nconf.set(settingKey, settingValue);
    nconf.save();
    ipc.send("config-change");
}

function readSettings(settingKey) {
    nconf.load();
    return nconf.get(settingKey);
}

function readSettings(settingKey, defaultValue) {
    nconf.load();
    var value = nconf.get(settingKey);
    if(value == undefined) {
        value = defaultValue;
    }
    return value;
}

function getUserHome() {
    return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}

function resetSettings() {
    nconf.saveSettings("enableReminder", true);
    nconf.saveSettings("remindEvery", 20);
    ipc.send("config-change");
}

module.exports = {
    saveSettings: saveSettings,
    readSettings: readSettings,
    resetSettings: resetSettings,
    DEFAULT_REMINDER_TIME: DEFAULT_REMINDER_TIME
}