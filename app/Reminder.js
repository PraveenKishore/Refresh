'use strict';

const {
    BrowserWindow
} = require("electron");
let electronVibrancy = null;
if (process.platform === "win32") {
    electronVibrancy = require("windows10-fluently-vibrancy");
}
const configuration = require("./configuration");

class Reminder {
    constructor() {
        this.window = null;
        this.previousReminderTime = new Date();
        // this.frequency = configuration.readSettings("remindEvery", configuration.DEFAULT_REMINDER_TIME); // 60; // Remind every x seconds
        this.intervalObj = null; // For storing result of setInterval, used later to clear the callback while exiting
    }

    startReminder() {
        this.frequency = configuration.readSettings("remindEvery", configuration.DEFAULT_REMINDER_TIME);
        console.log("Starting reminder: " + this.frequency);
        this.previousReminderTime = new Date();
        this.scheduleNextReminder(); // Schedule first reminder
    }

    showReminder() {
        if (this.window != null) {
            console.log("Reminder already showing");
            this.window.show();
            return;
        }
        this.window = new BrowserWindow({
            frame: false,
            transparent: true,
            skipTaskbar: true,
            show: false,
            "vibrancy": "dark",
            icon: "app/assets/icons/refresh_icon.png"
        });
        this.window.maximize();
        this.window.loadFile("physical-activity.html");
        this.window.setAlwaysOnTop(true, "floating");
        this.window.setVisibleOnAllWorkspaces(true);
        this.window.on("ready-to-show", () => {
            if (process.platform === "win32") {
                electronVibrancy.enableVibrancy(this.window);
            }
            console.log("Showing");
            this.window.show();
            if(process.argv.indexOf("--debug") > -1) { 
                this.window.openDevTools();
            }
        });

        this.window.on("activity-done", () => {
            // this.scheduleNextReminder();
            this.window.close();
        });
        this.window.on("activity-not-done", () => {
            // this.scheduleNextReminder();
            this.window.close();
        });
        this.window.on("close", () => {
            // console.log("Closing");
            this.scheduleNextReminder();
            this.window = null;
        });
    }

    scheduleNextReminder() {
        // Calculate the time left
        console.log("Setting reminder for: " + this.frequency + ". Time is: " + new Date());
        this.intervalObj = setTimeout(() => {
            this.showReminder();
        }, this.frequency * 60 * 1000); // Once only
        // Update this.previousReminderTime
    }

    restartReminder() {
        console.log("Restart reminder");
        this.stopReminder();
        this.startReminder();
    }

    stopReminder() {
        console.log("Stopping reminder");
        clearTimeout(this.intervalObj);
        if (this.window != null) {
            this.window.close();
        }
    }
}

module.exports = Reminder;