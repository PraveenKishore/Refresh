'use strict';

// const ipc = require("electron").ipcRenderer
const configuration = require("./app/configuration");

var inputRange = document.getElementsByClassName('range')[0],
    maxValue = 180, // the higher the smoother when dragging
    speed = 5,
    currValue, rafID;

// set min/max value
inputRange.min = 20;
inputRange.max = maxValue;

var container = document.querySelector("#switchContainer");

var enableReminder = configuration.readSettings("enableReminder", true);
var remindEvery = configuration.readSettings("remindEvery", inputRange.min);

if ((enableReminder == NaN || enableReminder == undefined) || !remindEvery) {
    enableReminder = true;
    remindEvery = inputRange.min;
    configuration.saveSettings("enableReminder", enableReminder);
    configuration.saveSettings("remindEvery", remindEvery);
}

inputRange.value = remindEvery;

updateTime();
setSwitchState(enableReminder);
console.log("enabled: " + enableReminder);

function setSwitchState(activated) {
    if (activated) {
        container.classList.remove("switchOff");
        container.classList += " switchOn";
        // enableReminder = true;
    } else {
        container.classList.remove("switchOn");
        container.classList += " switchOff";
        // enableReminder = false;
    }
    /*if (container.classList.contains("switchOn")) {
        container.classList.remove("switchOn");
        container.classList += " switchOff";
        enableReminder = false;
    } else {
        container.classList.remove("switchOff");
        container.classList += " switchOn";
        enableReminder = true;
    }*/
}

function onOffSwitch() {
    if (container.classList.contains("switchOn")) {
        container.classList.remove("switchOn");
        container.classList += " switchOff";
        enableReminder = false;
    } else {
        container.classList.remove("switchOff");
        container.classList += " switchOn";
        enableReminder = true;
    }
    configuration.saveSettings("enableReminder", enableReminder);
    console.log("Saving: " + enableReminder);
}
container.addEventListener("click", onOffSwitch, false);

function updateTime() {
    console.log("Remind every: " + remindEvery);
    document.getElementById("mins").innerHTML = remindEvery = inputRange.value ? inputRange.value : remindEvery;
    if (configuration.readSettings("remindEvery") != remindEvery) {
        configuration.saveSettings("remindEvery", remindEvery);
    }
}

// handle range animation
function animateHandler() {

    // calculate gradient transition
    var transX = currValue - maxValue;

    // update input range
    inputRange.value = currValue;

    //Change slide thumb color on mouse up
    if (currValue < 20) {
        inputRange.classList.remove('ltpurple');
    }
    if (currValue < 40) {
        inputRange.classList.remove('purple');
    }
    if (currValue < 60) {
        inputRange.classList.remove('pink');
    }

    // determine if we need to continue
    if (currValue > -1) {
        window.requestAnimationFrame(animateHandler);
    }

    // decrement value
    currValue = currValue - speed;
}

// bind events
inputRange.addEventListener('mousedown', updateTime, false);
inputRange.addEventListener('mousestart', updateTime, false);
inputRange.addEventListener('mouseup', updateTime, false);
inputRange.addEventListener('touchend', updateTime, false);
inputRange.addEventListener('mousemove', updateTime, false);

// move gradient
inputRange.addEventListener('input', function () {
    //Change slide thumb color on way up
    if (this.value > 20) {
        inputRange.classList.add('ltpurple');
    }
    if (this.value > 40) {
        inputRange.classList.add('purple');
    }
    if (this.value > 60) {
        inputRange.classList.add('pink');
    }

    //Change slide thumb color on way down
    if (this.value < 20) {
        inputRange.classList.remove('ltpurple');
    }
    if (this.value < 40) {
        inputRange.classList.remove('purple');
    }
    if (this.value < 60) {
        inputRange.classList.remove('pink');
    }
});