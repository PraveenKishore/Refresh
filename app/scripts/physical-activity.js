// <script src="scripts/index.js"></script>
const { BrowserWindow } = require("electron").remote

document.onreadystatechange = function () {
    if (document.readyState == "complete") {
         // Set up event listeners
         document.getElementById("positiveButton").addEventListener("click", function (e) {
            var window = BrowserWindow.getFocusedWindow();
            window.close();
       });
    }
};

bodymovin = require("bodymovin");
var animation = bodymovin.loadAnimation({
    container: document.getElementById('bm'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'app/assets/anims/walking.json'
  })