const {
    app,
    Tray,
    Menu,
    BrowserWindow
} = require("electron");
const electronVibrancy = require("windows10-fluently-vibrancy");
Reminder = require("./app/Reminder");

window = null;
reminder = null;

settings = {
    "previous": new Date(),
    "frequency": 0.5 // Every x minutes
}

function showReminder() {
    if (window != null) {
        window.show();
    }
}

settingsWindow = null;
app.isQuitting = false;

function init() {
    reminder = new Reminder();
    reminder.startReminder();
    var appIcon = new Tray(__dirname + "/app/assets/electron.ico");
    var trayMenuTemplate = [{
        label: "Habit",
        click: function () {
            reminder.showReminder()
        }
    }, {
        label: "Settings",
        click: function () {
            console.log("Settings");
            settingsWindow.show();
        }
    }, {
        label: "Quit",
        click: function () {
            console.log("close");
            reminder.stopReminder();
            app.isQuitting = true;
            app.quit();
        }
    }];
    var trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
    appIcon.setContextMenu(trayMenu);
    createSettingsWindow();
}

function createSettingsWindow() {
    if (settingsWindow != null) {
        return; // Open only one instance of settings
    }
    settingsWindow = new BrowserWindow({
        show: false
    });
    settingsWindow.setMenu(null);
    settingsWindow.loadFile("control-panel.html");
    settingsWindow.on("ready-to-show", function () {
        settingsWindow.show();
        // window.openDevTools();
    });
    settingsWindow.on("minimize", function (event) {
        // settingsWindow = null;
        event.preventDefault();
        settingsWindow.hide();
    });
    settingsWindow.on("close", function (event) {
        // settingsWindow = null;
        if (!app.isQuitting) {
            event.preventDefault();
            settingsWindow.minimize();
            return false;
        } else {
            settingsWindow = null;
        }
    });
}

function createWindow() {
    window = new BrowserWindow({
        transparent: true,
        // alwaysOnTop: true,
        frame: false,
        show: false,
    });
    window.maximize();
    window.setAlwaysOnTop(true, "floating");
    window.setVisibleOnAllWorkspaces(true);

    window.loadFile("main.html");
    window.on("ready-to-show", function () {
        if (process.platform === "win32") {
            electronVibrancy.enableVibrancy(window);
        }
        window.show();
        // window.openDevTools();
    });

    window.on("close", function (event) {
        console.log("close");
    });
}



// app.on("ready", createWindow);
app.on("ready", init);